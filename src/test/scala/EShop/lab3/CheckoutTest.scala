package EShop.lab3

import EShop.lab2.CartActor.{CloseCheckout, StartCheckout}
import EShop.lab2.Checkout.{PaymentStarted, ReceivePayment, SelectDeliveryMethod, SelectPayment}
import EShop.lab2.CheckoutFSM
import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import akka.util.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Await
import scala.concurrent.duration._

class CheckoutTest
  extends TestKit(ActorSystem("CheckoutTest"))
  with FlatSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with Matchers
  with ScalaFutures {

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  implicit val timeout: Timeout = 1.second

  it should "Send close confirmation to cart" in {
    val cart = TestProbe()
    val checkout = cart.childActorOf(Props(new CheckoutFSM(cart.testActor)))

    checkout ! StartCheckout

    val selectDeliveryFuture = checkout ? SelectDeliveryMethod("delivery")
    val selectDeliveryRes = Await.result(selectDeliveryFuture, timeout.duration)
    selectDeliveryRes shouldBe OrderManager.Done

    val selectPaymentFuture = checkout ? SelectPayment("payment")
    val selectPaymentRes = Await.result(selectPaymentFuture, timeout.duration)
    assert(selectPaymentRes.isInstanceOf[PaymentStarted])

    checkout ! ReceivePayment
    cart.expectMsg(CloseCheckout)
  }

}
