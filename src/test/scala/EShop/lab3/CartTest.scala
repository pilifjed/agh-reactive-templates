package EShop.lab3

import EShop.lab2.CartActor.{CheckoutStarted, StartCheckout}
import EShop.lab2.CartFSM.Status
import EShop.lab2.{Cart, CartActor, CartFSM}
import EShop.lab3.OrderManager.{AddItem, Done}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestFSMRef, TestKit}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.Await

class CartTest
  extends TestKit(ActorSystem("CartTest"))
  with FlatSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with Matchers
  with ScalaFutures {

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  implicit val timeout: Timeout = 1.second

  //use GetItems command which was added to make test easier
  it should "add item properly" in {
    val cart = TestActorRef(new CartActor())
    val addItem1Future = cart ? AddItem("rollerblades")
    val addItem1Res = Await.result(addItem1Future, timeout.duration)
    addItem1Res shouldBe Done

  }

  it should "be empty after adding and removing the same item" in {
    val cart = TestActorRef(new CartActor())
    val addItem1Future = cart ? AddItem("rollerblades")
    val addItem1Res = Await.result(addItem1Future, timeout.duration)
    addItem1Res shouldBe Done
    val remItem1Future = cart ? OrderManager.RemoveItem("rollerblades")
    val remItem1Res = Await.result(remItem1Future, timeout.duration)
    remItem1Res shouldBe Done
  }

  it should "start checkout" in {
    val cart = TestFSMRef[Status.Value, Cart, CartFSM](new CartFSM())

    val addItem1Future = cart ? AddItem("rollerblades")
    val addItem1Res = Await.result(addItem1Future, timeout.duration)
    addItem1Res shouldBe Done

    val startCheckout1Future = cart ? StartCheckout
    val startCheckout1Res = Await.result(startCheckout1Future, timeout.duration)
    assert(startCheckout1Res.isInstanceOf[CheckoutStarted])
  }
}
