package EShop.lab2

import EShop.lab2.CartFSM.Status
import EShop.lab3.OrderManager.{AddItem, Done, RemoveItem}
import akka.actor.{ActorRef, FSM, Props}

import scala.concurrent.duration._
import scala.language.postfixOps

object CartFSM {

  object Status extends Enumeration {
    type Status = Value
    val Empty, NonEmpty, InCheckout = Value
  }


  def props() = Props(new CartFSM())
}

class CartFSM extends FSM[Status.Value, Cart] {
  import EShop.lab2.CartFSM.Status._
  import EShop.lab2.CartActor._
  // useful for debugging, see: https://doc.akka.io/docs/akka/current/fsm.html#rolling-event-log
  //override def logDepth = 12

  val cartTimerDuration: FiniteDuration = 1 seconds

  startWith(Empty, Cart.empty)

  when(Empty) {
    case Event(AddItem(item), cart) =>
      log.debug("Item: \"" + item + "\" added")
      sender ! Done
      goto(NonEmpty).using(cart.addItem(item))
  }

  when(NonEmpty, stateTimeout = cartTimerDuration) {
    case Event(AddItem(item), cart) =>
      log.debug("Item: \"" + item + "\" added")
      stay().using(cart.addItem(item)).replying(Done)
    case Event(RemoveItem(item), cart) if cart.removeItem(item).size >= 1 =>
      log.debug("Item: \"" + item + "\" removed")
      stay().using(cart.removeItem(item)).replying(Done)
    case Event(RemoveItem(item), cart) if cart.removeItem(item).size < 1 =>
      log.debug("Item: \"" + item + "\" removed")
      goto(Empty).using(Cart.empty).replying(Done)
    case Event(StateTimeout, _) =>
      log.debug("Cart expired")
      goto(Empty).using(Cart.empty)
    case Event(StartCheckout, cart) =>
      log.debug("Checkout started")
      val checkout : ActorRef = context.actorOf(Props(new CheckoutFSM(context.self)))
      checkout ! StartCheckout
      goto(InCheckout).using(cart).replying(CheckoutStarted(checkout))
  }

  when(InCheckout) {
    case Event(CancelCheckout, cart) =>
      log.debug("Checkout cancelled")
      context.parent ! CheckoutCancelled
      goto(NonEmpty).using(cart)
    case Event(CloseCheckout, _) =>
      log.debug("Checkout closed")
      //context.stop(sender()) // comment line for tests
      goto(Empty).using(Cart.empty)
  }
}
