package EShop.lab2

import EShop.lab2.CartActor.{CancelCheckout, CloseCheckout, StartCheckout}
import EShop.lab2.Checkout.{Command, ExpireCheckout}
import EShop.lab3.OrderManager.Done
import EShop.lab3.Payment
import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.Logging

import scala.concurrent.duration._
import scala.language.postfixOps

object Checkout {

  sealed trait Data
  case object Uninitialized                               extends Data
  case class SelectingDeliveryStarted(timer: Cancellable) extends Data
  case class ProcessingPaymentStarted(timer: Cancellable) extends Data

  sealed trait Command
  //case object StartCheckout                       extends Command
  case class SelectDeliveryMethod(method: String) extends Command
  case object ExpireCheckout                      extends Command
  case class SelectPayment(payment: String)       extends Command
  case object ExpirePayment                       extends Command
  case object ReceivePayment                      extends Command
  case object Expire                              extends Command

  sealed trait Event
  case object CheckOutClosed                        extends Event
  case class PaymentStarted(payment: ActorRef)      extends Event
  case object CheckoutStarted                       extends Event
  case object CheckoutCancelled                     extends Event
  case class DeliveryMethodSelected(method: String) extends Event

  def props(cart: ActorRef) = Props(new Checkout(cart))
}

class Checkout(cartRef: ActorRef) extends Actor {
  import EShop.lab2.Checkout._
  private val scheduler = context.system.scheduler
  private val log       = Logging(context.system, this)

  val checkoutTimerDuration: FiniteDuration = 1 seconds
  val paymentTimerDuration: FiniteDuration = 1 seconds

  private def scheduleTimer(duration: FiniteDuration, message: Command): Cancellable =
    scheduler.scheduleOnce(duration, context.self, message)(context.dispatcher)

  def receive: Receive = {
    case StartCheckout =>
      val timer = scheduleTimer(checkoutTimerDuration, ExpireCheckout)
      context.become(selectingDelivery(timer))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case CancelCheckout | ExpireCheckout =>
      cartRef ! CancelCheckout
      context.become(cancelled)
    case SelectDeliveryMethod(method) =>
      log.debug("Selected: \"" + method + "\" delivery method")
      sender() ! Done
      context.become(selectingPaymentMethod(timer))
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case CancelCheckout | ExpireCheckout =>
      context.become(cancelled)
      cartRef ! CancelCheckout
    case SelectPayment(paymentMethod) =>
      timer.cancel()
      log.debug("Selected: \"" + paymentMethod + "\" payment method")
      val payment = context.actorOf(Props(new Payment(paymentMethod, context.sender(), context.self)))
      sender() ! PaymentStarted(payment)
      context.become(processingPayment(scheduleTimer(paymentTimerDuration, ExpirePayment)))
  }

  def processingPayment(timer: Cancellable): Receive = {
    case CancelCheckout | ExpirePayment =>
      context.become(cancelled)
      cartRef ! CancelCheckout
    case ReceivePayment =>
      context.become(closed)
      //context.stop(sender()) // comment for tests
      cartRef ! CloseCheckout
  }

  def cancelled: Receive = {
    case _ =>
  }

  def closed: Receive = {
    case _ =>
  }
}
