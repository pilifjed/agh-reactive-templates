package EShop.lab2

import EShop.lab2.CartActor.{CancelCheckout, CloseCheckout, StartCheckout}
import EShop.lab2.Checkout.Data
import EShop.lab2.CheckoutFSM.Status
import EShop.lab3.OrderManager.Done
import EShop.lab3.{Payment, PaymentFSM}
import akka.actor.{ActorRef, Cancellable, FSM, Props}

import scala.concurrent.duration._
import scala.language.postfixOps

object CheckoutFSM {

  object Status extends Enumeration {
    type Status = Value
    val NotStarted, SelectingDelivery, SelectingPaymentMethod, Cancelled, ProcessingPayment, Closed = Value
  }

  def props(cartActor: ActorRef) = Props(new CheckoutFSM(cartActor))
}

class CheckoutFSM(cartActor: ActorRef) extends FSM[Status.Value, Data] {
  import EShop.lab2.CheckoutFSM.Status._
  import EShop.lab2.Checkout._
  // useful for debugging, see: https://doc.akka.io/docs/akka/current/fsm.html#rolling-event-log
  //override def logDepth = 12

  val checkoutTimerDuration: FiniteDuration = 1 seconds
  val paymentTimerDuration: FiniteDuration  = 1 seconds

  private val scheduler = context.system.scheduler

  private def scheduleTimer(duration: FiniteDuration, message: Command): Cancellable =
    scheduler.scheduleOnce(duration, context.self, message)(context.dispatcher)

  startWith(NotStarted, Uninitialized)

  when(NotStarted){
    case Event(StartCheckout, Uninitialized) =>
      goto(SelectingDelivery).using(SelectingDeliveryStarted(scheduleTimer(checkoutTimerDuration, ExpireCheckout)))
  }

  when(SelectingDelivery) {
    case Event(SelectDeliveryMethod(method), SelectingDeliveryStarted(timer)) =>
      log.debug("Selected: \"" + method + "\" delivery method")
      goto(SelectingPaymentMethod).using(SelectingDeliveryStarted(timer)).replying(Done)
    case Event(ExpireCheckout, _) | Event(CancelCheckout, _) =>
      cartActor ! CancelCheckout
      goto(Cancelled).using(Uninitialized)
  }

  when(SelectingPaymentMethod) {
    case Event(SelectPayment(paymentMethod), SelectingDeliveryStarted(timer)) =>
      timer.cancel()
      log.debug("Selected: \"" + paymentMethod + "\" delivery method")
      val payment = context.actorOf(Props(new PaymentFSM(paymentMethod, context.sender(), context.self)))
      goto(ProcessingPayment)
        .using(ProcessingPaymentStarted(scheduleTimer(paymentTimerDuration, ExpirePayment)))
        .replying(PaymentStarted(payment))
    case Event(ExpireCheckout, _) | Event(CancelCheckout, _) =>
      cartActor ! CancelCheckout
      goto(Cancelled).using(Uninitialized)
  }

  when(ProcessingPayment) {
    case Event(ExpirePayment, _) | Event(CancelCheckout, _) =>
      context.parent ! CancelCheckout
      goto(Cancelled).using(Uninitialized)
    case Event(ReceivePayment, ProcessingPaymentStarted(timer)) =>
      timer.cancel()
      //context.stop(sender()) // comment for tests
      cartActor ! CloseCheckout
      goto(Closed).using(Uninitialized)
  }

  when(Cancelled) {
    case _ =>
      stay()
  }

  when(Closed) {
    case _ =>
      stay()
  }

}
