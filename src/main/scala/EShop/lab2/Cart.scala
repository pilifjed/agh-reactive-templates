package EShop.lab2

case class Cart(items: Seq[Any]) {
  def contains(item: Any): Boolean = items.contains(item)
  def addItem(item: Any): Cart     = Cart(items :+ item)
  def removeItem(item: Any): Cart  = {
    val idx = items.indexOf(item)
    if (idx < 0) {
      this
    }
    else if (idx == 0){
      Cart(items.tail)
    }
    else{
      val (l, r) = items.splitAt(idx)
      Cart(l ++ r.tail)
    }
  }
  def size: Int = items.size
}

object Cart {
  def empty: Cart = Cart(Seq.empty)
}
