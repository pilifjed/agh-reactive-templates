package EShop.lab2

import EShop.lab3.OrderManager.{AddItem, Done, RemoveItem}
import akka.actor.{Actor, ActorRef, Cancellable, Props, Timers}
import akka.event.Logging

import scala.concurrent.duration._
import scala.language.postfixOps

object CartActor {

  sealed trait Command
  //case class AddItem(item: Any)    extends Command
  //case class RemoveItem(item: Any) extends Command
  case object ExpireCart           extends Command
  case object StartCheckout        extends Command
  case object CancelCheckout       extends Command
  case object CloseCheckout        extends Command

  sealed trait Event
  case class CheckoutStarted(checkoutRef: ActorRef) extends Event
  case object CheckoutCancelled extends Event

  sealed trait Data
  case class CartState(size: Int) extends Data

  def props = Props(new CartActor())
}

class CartActor extends Actor {
  import CartActor._
  private val log       = Logging(context.system, this)
  private val scheduler = context.system.scheduler
  val cartTimerDuration : FiniteDuration = 1 seconds

  private def scheduleTimer: Cancellable =
    scheduler.scheduleOnce(cartTimerDuration, context.self, ExpireCart)(context.dispatcher)

  def receive: Receive = empty

  def empty: Receive = {
    case AddItem(item) =>
      log.debug("Item: \"" + item + "\" added")
      sender() ! Done
      context.become(nonEmpty(Cart(List(item)), scheduleTimer))
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case AddItem(item) =>
      log.debug("Item: \"" + item + "\" added")
      sender() ! Done
      context.become(nonEmpty(cart.addItem(item), timer))
    case RemoveItem(item) if cart.removeItem(item).size < 1  =>
      log.debug("Item: \"" + item + "\" removed")
      sender() ! Done
      context.become(empty)
    case RemoveItem(item) if cart.removeItem(item).size >= 1 =>
      log.debug("Item: \"" + item + "\" removed")
      sender() ! Done
      context.become(nonEmpty(cart.removeItem(item), timer))
    case ExpireCart =>
      log.debug("Cart expired")
      context.become(empty)
    case StartCheckout =>
      timer.cancel()
      log.debug("Checkout started")
      val checkout : ActorRef = context.actorOf(Props(new Checkout(context.self)))
      checkout ! StartCheckout
      sender() ! CheckoutStarted(checkout)
      context.become(inCheckout(cart))
  }

  def inCheckout(cart: Cart): Receive = {
    case CancelCheckout =>
      log.debug("Checkout cancelled")
      context.parent ! CheckoutCancelled
      context.become(nonEmpty(cart, scheduleTimer))
    case CloseCheckout =>
      log.debug("Checkout closed")
      //context.stop(sender()) // comment line for tests
      context.become(empty)
  }
}
