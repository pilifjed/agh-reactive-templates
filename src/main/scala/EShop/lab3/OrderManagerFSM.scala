package EShop.lab3

import EShop.lab2.CartActor.{CheckoutStarted, StartCheckout}
import EShop.lab2.{CartActor, CartFSM}
import EShop.lab2.Checkout.{PaymentStarted, SelectDeliveryMethod, SelectPayment}
import EShop.lab3.OrderManager._
import EShop.lab3.Payment.{DoPayment, PaymentConfirmed}
import akka.actor.{FSM, Props}
import akka.util.Timeout
import akka.pattern.ask

import scala.language.postfixOps
import scala.concurrent.Await
import scala.concurrent.duration._

class OrderManagerFSM extends FSM[State, Data] {

  startWith(Uninitialized, Empty)

  when(Uninitialized) {
    case Event(command: AddItem, Empty) =>
      val cart = context.actorOf(Props[CartFSM])
      cart ! command
      goto(Open).using(CartDataWithSender(cart, sender()))
  }

  when(Open) {
    case Event(command: AddItem, data : CartDataWithSender) =>
      data.cartRef ! command
      stay()
    case Event(command: RemoveItem, data : CartDataWithSender) =>
      data.cartRef ! command
      stay()
    case Event(Buy, data: CartDataWithSender) =>
      data.cartRef ! StartCheckout
      goto(InCheckout).using(InCheckoutData(sender()))
    case Event(Done, data: CartDataWithSender) =>
      data.sender ! Done
      stay()
  }

  when(InCheckout) {
    case Event(CheckoutStarted(checkoutRef), data: InCheckoutData) =>
      data.sender ! Done
      stay().using(InCheckoutDataWithCheckout(checkoutRef,data.sender))
    case Event(SelectDeliveryAndPaymentMethod(delivery,payment), data: InCheckoutDataWithCheckout) =>
      implicit val timeout : akka.util.Timeout = Timeout(5 seconds)
      val future = data.checkoutRef ? SelectDeliveryMethod(delivery)
      Await.result(future, timeout.duration)
      data.checkoutRef ! SelectPayment(payment)
      goto(InPayment).using(InPaymentData(sender()))
  }

  when(InPayment) {
    case Event(PaymentStarted(paymentRef), data: InPaymentData) =>
      data.sender ! Done
      stay().using(InPaymentDataWithPayment(paymentRef, data.sender))
    case Event(Pay, data: InPaymentDataWithPayment) =>
      data.paymentRef ! DoPayment
      stay().using(InPaymentDataWithPayment(data.paymentRef, sender()))
    case Event(PaymentConfirmed, data: InPaymentDataWithPayment) =>
      data.sender ! Done
      goto(Finished).using(Empty)
  }

  when(Finished) {
    case _ =>
      sender ! "order manager finished job"
      stay()
  }

}
