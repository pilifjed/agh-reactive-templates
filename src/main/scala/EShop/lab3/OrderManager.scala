package EShop.lab3

import EShop.lab2.CartActor.StartCheckout
import EShop.lab2.Checkout.{SelectDeliveryMethod, SelectPayment}
import EShop.lab2.{CartActor, Checkout}
import EShop.lab3.OrderManager._
import EShop.lab3.Payment.{DoPayment, PaymentConfirmed}
import akka.actor.{Actor, ActorRef, Props}
import akka.event.{Logging, LoggingReceive}
import akka.util.Timeout
import akka.pattern.ask

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

object OrderManager {
  sealed trait State
  case object Uninitialized extends State
  case object Open          extends State
  case object InCheckout    extends State
  case object InPayment     extends State
  case object Finished      extends State

  sealed trait Command
  case object Start                                                            extends Command
  case class AddItem(id: String)                                               extends Command
  case class RemoveItem(id: String)                                            extends Command
  case class SelectDeliveryAndPaymentMethod(delivery: String, payment: String) extends Command
  case object Buy                                                              extends Command
  case object Pay                                                              extends Command

  sealed trait Ack
  case object Done extends Ack //trivial ACK

  sealed trait Data
  case object Empty                                                            extends Data
  case class CartData(cartRef: ActorRef)                                       extends Data
  case class CartDataWithSender(cartRef: ActorRef, sender: ActorRef)           extends Data
  case class InCheckoutData(sender: ActorRef)                             extends Data
  case class InCheckoutDataWithCheckout(checkoutRef: ActorRef, sender: ActorRef) extends Data
  case class InPaymentData(sender: ActorRef)                               extends Data
  case class InPaymentDataWithPayment(paymentRef: ActorRef, sender: ActorRef)   extends Data
}

class OrderManager extends Actor {

  private val log       = Logging(context.system, this)
  override def receive: Receive = uninitialized

  def uninitialized: Receive = {
    case command: AddItem =>
      log.debug("Adding item")
      val cart = context.actorOf(Props[CartActor])
      cart ! command
      context.become(open(sender(), cart))
  }

  def open(master: ActorRef, cartActor: ActorRef): Receive = {
    case command: AddItem =>
      log.debug("Adding item")
      cartActor ! command
//      context.become(open(sender(), cartActor))
    case command: RemoveItem =>
      log.debug("Removing item")
      cartActor ! command
//      context.become(open(sender(), cartActor))
    case Buy =>
      log.debug("Starting checkout")
      cartActor ! StartCheckout
      context.become(inCheckout(sender()))
    case Done =>
      log.debug("Done")
      master ! Done
  }

  def inCheckout(senderRef: ActorRef): Receive = {
    case CartActor.CheckoutStarted(checkoutRef) => {
      senderRef ! Done
      context.become(inCheckout(senderRef, checkoutRef))
    }
  }

  def inCheckout(master: ActorRef,checkoutActorRef: ActorRef): Receive = {
    case SelectDeliveryAndPaymentMethod(delivery, payment) => {
      implicit val timeout : akka.util.Timeout = Timeout(5 seconds)
      val future = checkoutActorRef ? SelectDeliveryMethod(delivery)
      Await.result(future, timeout.duration)
      checkoutActorRef ! SelectPayment(payment)
      context.become(inPayment(sender()))
    }
  }

  def inPayment(senderRef: ActorRef): Receive = {
    case Checkout.PaymentStarted(paymentRef) =>
      senderRef ! Done
      context.become(inPayment(paymentRef, senderRef))
  }

  def inPayment(paymentActorRef: ActorRef, senderRef: ActorRef): Receive = {
    case Pay =>
      paymentActorRef ! DoPayment
      context.become(inPayment(paymentActorRef, sender()))
    case PaymentConfirmed =>
      senderRef ! Done
      context.become(finished)
  }

  def finished: Receive = {
    case _ => sender ! "order manager finished job"
  }
}
