package EShop.lab4

import EShop.lab2.CartActor.{CancelCheckout, CheckoutStarted, CloseCheckout, ExpireCart, StartCheckout}
import EShop.lab2.{Cart, Checkout}
import EShop.lab3.OrderManager.{AddItem, Done, RemoveItem}
import akka.actor.{ActorRef, Cancellable, Props}
import akka.event.{Logging, LoggingReceive}
import akka.persistence.PersistentActor

import scala.concurrent.duration._

object PersistentCartActor {

  sealed trait Evt
  case object CartExpired extends Evt
  case object CheckoutClosed extends Evt
  case class CheckoutCancelled(cart: Cart) extends Evt
  case class ItemAdded(item: Any, cart: Cart) extends Evt
  case object CartEmptied extends Evt
  case class ItemRemoved(item: Any, cart: Cart) extends Evt
  case class CheckoutStartedEvent(checkoutRef: ActorRef, cart: Cart) extends Evt


  def props(persistenceId: String) = Props(new PersistentCartActor(persistenceId))
}

class PersistentCartActor(val persistenceId: String) extends PersistentActor {
  import EShop.lab4.PersistentCartActor._

  private val log       = Logging(context.system, this)
  private val scheduler = context.system.scheduler
  val cartTimerDuration: FiniteDuration = 5.seconds
  private def scheduleTimer: Cancellable =
    scheduler.scheduleOnce(cartTimerDuration, context.self, ExpireCart)(context.dispatcher)

  override def receiveCommand: Receive = empty

  private def updateState(event: Evt, timer: Option[Cancellable] = None): Unit = {
    context.become(
    event match {
      case ItemAdded(item, cart) =>
        nonEmpty(cart.addItem(item), timer.getOrElse(scheduleTimer))
      case ItemRemoved(item, cart) if cart.removeItem(item).size >= 1 =>
        nonEmpty(cart.removeItem(item), timer.get)
      case ItemRemoved(item, cart) if cart.removeItem(item).size < 1 =>
        timer.get.cancel()
        empty
      case CheckoutStartedEvent(checkoutRef, cart) =>
        inCheckout(cart)
      case CartExpired | CheckoutClosed =>
        empty
      case CheckoutCancelled(cart) =>
        nonEmpty(cart, timer.getOrElse(scheduleTimer))
    })
  }

  def empty: Receive = {
    case AddItem(item) =>
      persist(ItemAdded(item, Cart.empty)) {
       event =>
         log.debug("Item: \"" + item + "\" added")
         sender() ! Done
         updateState(event)
      }
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case AddItem(item) =>
      persist(ItemAdded(item, cart)) {
        event =>
          log.debug("Item: \"" + item + "\" added")
          sender() ! Done
          updateState(event, Some(timer))
      }
    case RemoveItem(item) =>
      persist(ItemRemoved(item, cart)) {
        event =>
        log.debug("Item: \"" + item + "\" removed")
        sender() ! Done
        updateState(event, Some(timer))
      }
    case ExpireCart =>
      persist(CartExpired) {
        event =>
          log.debug("Cart expired")
          updateState(event, Some(timer))
      }
    case StartCheckout =>
      log.debug("Checkout started")
      val checkout : ActorRef = context.actorOf(Props(new Checkout(context.self)))
      persist(CheckoutStartedEvent(checkout, cart)) {
        event =>
          timer.cancel()
          checkout ! StartCheckout
          sender() ! CheckoutStarted(checkout)
          updateState(event)
      }
  }

  def inCheckout(cart: Cart): Receive = {
    case CancelCheckout =>
      persist(CheckoutCancelled(cart)) {
        event =>
          log.debug("Checkout cancelled")
          context.parent ! CheckoutCancelled
          updateState(event, Some(scheduleTimer))
      }
    case CloseCheckout =>
      persist(CheckoutClosed) {
        event =>
          log.debug("Checkout closed")
          //context.stop(sender()) // comment line for tests
          updateState(event)
      }
  }

  override def receiveRecover: Receive = {
    case event: Evt => updateState(event)
  }
}
