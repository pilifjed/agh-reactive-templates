package EShop.lab4

import EShop.lab2.CartActor
import EShop.lab2.CartActor.{CancelCheckout, CloseCheckout, StartCheckout}
import EShop.lab3.OrderManager.Done
import EShop.lab3.Payment
import akka.actor.{ActorRef, Cancellable, Props}
import akka.event.{Logging, LoggingReceive}
import akka.persistence.PersistentActor

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object PersistentCheckout {

  sealed trait Evt
  case object CheckoutStartedEvent extends Evt
  case class DeliveryMethodSelectedEvent(method: String) extends Evt
  case object CheckOutClosedEvent extends Evt
  case object CheckoutCancelledEvent extends Evt
  case class PaymentStartedEvent(payment: ActorRef) extends Evt

  def props(cartActor: ActorRef, persistenceId: String) =
    Props(new PersistentCheckout(cartActor, persistenceId))
}

class PersistentCheckout(
  cartActor: ActorRef,
  val persistenceId: String
) extends PersistentActor {

  import EShop.lab2.Checkout._
  import PersistentCheckout._
  private val scheduler = context.system.scheduler
  private val log       = Logging(context.system, this)
  val timerDuration: FiniteDuration    = 1.seconds

  private def scheduleTimer(message: Command): Cancellable =
    scheduler.scheduleOnce(timerDuration, context.self, message)(context.dispatcher)

  private def updateState(event: Evt, maybeTimer: Option[Cancellable] = None): Unit = {
    context.become(
      event match {
        case CheckoutStartedEvent =>
          selectingDelivery(scheduleTimer(ExpireCheckout))
        case DeliveryMethodSelectedEvent(method) =>
          selectingPaymentMethod(maybeTimer.getOrElse(scheduleTimer(ExpireCheckout)))
        case CheckOutClosedEvent =>
          closed
        case CheckoutCancelledEvent =>
          cancelled
        case PaymentStartedEvent(payment) =>
          processingPayment(scheduleTimer(ExpirePayment))
      }
    )
  }

  def receiveCommand: Receive = {
    case StartCheckout =>
      persist(CheckoutStartedEvent) {
        event => updateState(event)
      }
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case CancelCheckout | ExpireCheckout =>
      persist(CheckoutCancelledEvent) {
        event =>
          cartActor ! CancelCheckout
          updateState(event)
      }
    case SelectDeliveryMethod(method) =>
      persist(DeliveryMethodSelectedEvent(method)) {
        event =>
          log.debug("Selected: \"" + method + "\" delivery method")
          sender() ! Done
          updateState(event)
      }
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case CancelCheckout | ExpireCheckout =>
      persist(CheckoutCancelledEvent) {
        event =>
          cartActor ! CancelCheckout
          updateState(event)
      }
    case SelectPayment(paymentMethod) =>
      val payment = context.actorOf(Props(new Payment(paymentMethod, context.sender(), context.self)))
      persist(PaymentStartedEvent(payment)){
        event =>
          timer.cancel()
          sender() ! PaymentStarted(payment)
          updateState(event)
      }
  }

  def processingPayment(timer: Cancellable): Receive = {
    case CancelCheckout | ExpirePayment =>
      persist(CheckoutCancelledEvent) {
        event =>
          cartActor ! CancelCheckout
          updateState(event)
      }
    case ReceivePayment =>
      persist(CheckOutClosedEvent) {
        event =>
          cartActor ! CloseCheckout
          updateState(event)
      }
  }

  def cancelled: Receive = {
    case _ =>
  }

  def closed: Receive = {
    case _ =>
  }

  override def receiveRecover: Receive = {
    case event: Evt => updateState(event)
  }
}
